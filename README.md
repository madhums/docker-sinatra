A hello world sinatra json api running on docker

```sh
$ docker-compose up -d
```

Changes to `/app` will be automatically picked up by shotgun

Visit [http://localhost:4000](http://localhost:4000)
