require "sinatra/base"
require "sinatra/json"

class Web < Sinatra::Base
  set :bind, "0.0.0.0" # this is important for running in docker

  get "/" do
    json :foo => "bar"
  end
end
