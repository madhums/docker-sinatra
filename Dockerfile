FROM ruby

WORKDIR /app
COPY . /app
RUN bundle install

EXPOSE 4567

# Providing --host 0.0.0.0 is important here for running in docker
# The same must also be bound in sinatra config using set :bind, '0.0.0.0'
CMD bundle exec shotgun --port 4567 --host 0.0.0.0 --server thin config.ru
